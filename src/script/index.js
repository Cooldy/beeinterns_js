// я изменила немного название переменных, чтобы в одном файле у меня не было ошибок

// 1. Разобраться, что выводит код и написать объяснение в комментарий

let myVar; //объявили переменную myVar
myVar = 5; //присвоили myVar значение 5
let myNum; //объявили переменную myNum
myNum = myVar; //присвоили myNum значение переменной myVar
myVar = 10; //присвоили myVar 10
myNum = myVar; //присвоили myNum значение переменной myVar
//в итоге обе переменные равны 10

/** Обьяснить в комментарии к коду, что произошло */

// 2. Разобраться, что выводит код и написать объяснение в комментарий

// const myVarTwo;
// myVarTwo = 5;
// const myNumTwo;
// myNumTwo = myVarTwo;
// myVarTwo = 10;
// myNumTwo = myVarTwo;

/*переменная объявленная через сonst должна быть сразу инициализирована,
 в данном случае они только объявлены и появляется предупреждение. Перепишу так:  */
const myVarTwo = 5;
const myNumTwo = myVarTwo;
// myVarTwo = 10;
// myNumTwo = myVarTwo;

/*в строчках 26 и 27 мы хотели переопределить наши const переменные, в итоге выводится ошибка
так как нельзя переопределить const переменную
*/
/** Обьяснить в комментарии к коду, что произошло */

// 3. Разобраться, что выводит код и написать объяснение в комментарий
let a = 123;
let b = -123;
let c = "Hello";
const e = 123;
//Объявили и проинициализировали перменные
a = a + 1; // к текущему а(123) + 1, а = 124
b = +5; //переопределили b, b=5
c = c + " world!"; // к текущему c("Hello") + " world!", с = Hello world!
// e = e + 123;//попытка переопределить const переменную - ошибка

/** Обьяснить в комментарии к коду, что произошло */

// 4. Разобраться, что выводит код и написать объяснение в комментарий

let aFour = 0; //Объявили и проинициализировали aFour
let bFour; //Объявили bFour

if (aFour) {
  console.log(aFour);
} else if (bFour) {
  console.log(bFour);
} else {
  console.log("hello world");
}

/** Обьяснить в комментарии к коду, что произошло */

/*выполненяется действие в зависимости от условия, 
if(...) вычисляет условие в скобках и, если результат true, то выполняет блок кода.
в данном случае aFour = 0 , а это интерпретируется как false,  bFour не объявлено, что также будет false.
Поэтому выполнятся последний блок и выводится 'hello world'.
*/

// 5. Необходимо проверить на тип переменную a. Посмотреть, что выводит код и написать объяснение в комментарий

//Для проверки типа переменной используем оепратор  typeof.

let aFive = "Hello world"; //Объявили и проинициализировали aFive

if (typeof aFive === "string") {
  console.log(aFive); //aFive действительно строка, в консоли будет - "Hello world"
}

// 6. Разобраться, что выводит код и написать объяснение в комментарий

let aSix = {}; //обявляем и инициализируем пустой объект
let bSix = aSix; //обявляем переменную bSix и присваиваем ей значение aSix,

alert(aSix == bSix); //сравнение по значению
alert(aSix === bSix); //сравнение по значению и по типу

//в обоих случаях выведется true, так как объект это ссылочный тип данных,в bSix лежит именно ссылка на объект aSix, и следоватлеьно они равны*/
/** Обьяснить в комментарии к коду, что произошло */
